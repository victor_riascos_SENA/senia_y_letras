﻿using UnityEngine;
using System.Collections;

public enum UISounds { Button, Musica }

public class SoundManager : MonoBehaviour
{
	private static SoundManager     instance = null;
    
    private AudioSource             musicSource;
    public  AudioSource[]           uiSounds;

    public static SoundManager Instance
	{
		get { return instance; }
	}

	void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
			return;
		}
		else
		{
			instance = this;
		}

		DontDestroyOnLoad (this.gameObject);
	}

    void Start()
    {
        musicSource = this.GetComponent<AudioSource>();
    }

    public void PlayMusic(bool isOn)
    {
        if (isOn == true) { musicSource.Play(); }
        else
        musicSource.Stop();
    }

    public void PlayUISounds(UISounds soundToPlay, bool play)
    {
        if (!GameInformation.SeniasYLetras.Sonido) return;

        if (!uiSounds[(int)soundToPlay].isPlaying && play) uiSounds[(int)soundToPlay].Play();
        else if (!play) uiSounds[(int)soundToPlay].Stop();
    }
}
