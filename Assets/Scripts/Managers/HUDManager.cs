﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// HUD manager. Es un patrón singletón (solo puede existir uno),
/// que le hereda a Monobehavior
/// </summary>
public class HUDManager : MonoBehaviour
{
	private static HUDManager 	instance = null;
	public Pantalla				pantallaActual = Pantalla.Menu;
    public CanvasGroup          cnvGrp_MenuDicionario;
    public CanvasGroup			cnvGrp_MenuPrincipal;
	public Animator 			animatorPantallaSalirApp;
    public Animator             animatorMenuPrincipal;
	public Animator             animatorPnlSubtitulo;
	public Text 				txt_subtitulo;
<<<<<<< HEAD
    //public Animator             animatorVolverMenu;
=======
<<<<<<< HEAD
	public ModoAplicacion 		modoActual = ModoAplicacion.Diccionario;
    //public Animator             animatorVolverMenu;
=======
    public Animator             animatorTools;
    public Animator             animatorCreditos;
>>>>>>> 5094953677024c14587bb542a925949348850966
>>>>>>> af6ba9e950ae67fa05673b8b59acedb0a837bc32

    public Animator             animatorPnl_VolverMenu;
    public Animator             animatorMenuEmer;

	private string 				nombreAnimacion;

<<<<<<< HEAD
	public string NombreAnimacion {
		get {
			return nombreAnimacion;
		}
		set {
			nombreAnimacion = value;
		}
=======
	public string NombreAnimacion 
	{
		get {return nombreAnimacion;}
		set {nombreAnimacion = value;}
>>>>>>> af6ba9e950ae67fa05673b8b59acedb0a837bc32
	}


    public static HUDManager Instance
	{
		get { return instance; }
		set { instance = value; }
	}

	void Awake()
	{
		// Si ya se reservó memoria a la instancia
		// y no es esta instancia
		if (Instance != null && Instance != this)
		{
			Destroy(this.gameObject);
			return;
		}

		Instance = this;
	}

	public void ActivarPantallaSalirApp(bool activar)
	{
		cnvGrp_MenuPrincipal.interactable = !activar;
		animatorPantallaSalirApp.SetBool("activar", activar);
	}

    public void ActivarPantallaDiccionario(bool activar)
    {
       
        animatorMenuPrincipal.SetBool("activar", activar);

		animatorPnlSubtitulo.SetBool("activar", activar);
<<<<<<< HEAD

=======
>>>>>>> 5094953677024c14587bb542a925949348850966


        //if (activar == true)
        //{
        //    Debug.LogError("activa el boton volver");
        //    animatorPnl_VolverMenu.SetBool("mostrar", true);
        //}

    }

    public void ActivarBotonVolverMenu(bool mostrar)
    {
<<<<<<< HEAD

        animatorPnl_VolverMenu.SetBool("activar", mostrar);
=======
<<<<<<< HEAD

        animatorPnl_VolverMenu.SetBool("activar", mostrar);
=======
        
        animatorPnl_VolverMenu.SetBool("activar", mostrar);

>>>>>>> 5094953677024c14587bb542a925949348850966
>>>>>>> af6ba9e950ae67fa05673b8b59acedb0a837bc32
    }
    public void ActivarPantallaEmerMenu(bool activar)
    {
        animatorMenuEmer.SetBool("activar", activar);
        cnvGrp_MenuDicionario.interactable = !activar;
<<<<<<< HEAD
=======
    }
    public void ActivarPantallaCredito(bool activar)
    {

        animatorCreditos.SetBool("activar", activar);
        cnvGrp_MenuDicionario.interactable = !activar;
    }

    public void ActivarPanelTools(bool activar)
    {
        animatorTools.SetBool("activar", activar);
        
>>>>>>> 5094953677024c14587bb542a925949348850966
    }

<<<<<<< HEAD
	public void MostrarSubtitulo ()
=======
    public void MostrarSubtitulo ()
>>>>>>> af6ba9e950ae67fa05673b8b59acedb0a837bc32
	{
		txt_subtitulo.text = nombreAnimacion;
	}
}
 