﻿using System.Collections.Generic;

public class Personaje
{
	private Genero 							generoPersonaje = Genero.Masculino;
	private SerializableDictionary<string, Animacion>	animaciones = new SerializableDictionary<string, Animacion>();

	public Genero GeneroPersonaje
	{
		get { return generoPersonaje; }
		set { generoPersonaje = value; }
	}

	public SerializableDictionary<string, Animacion> Animaciones
	{
		get { return animaciones; }
		set { animaciones = value; }
	}
}
