﻿
public static class UnityUtility
{
	private static bool		estaPausado = false;
	private static float 	tiempoUI	= 1.0f;
	private static float 	tiempoJuego	= 1.0f;

	public static bool EstaPausado
	{
		get { return estaPausado; }
		set { estaPausado = value; }
	}

	public static float TiempoUI
	{
		get { return tiempoUI; }
	}

	/// <summary>
	/// Si esta pausado entonces retorna cero (0.0f) para que ningún objeto 3D o sprite se mueva, sino,
	/// retornará el tiempo de juego actual
	/// </summary>
	/// <value>The tiempo juego.</value>
	public static float TiempoJuego
	{
		get { return ((estaPausado == true) ? 0.0f: tiempoJuego); }
		set { tiempoJuego = value; }
	}

	/// <summary>
	/// Se cambian los valores por defecto de los atributos de UnityUtility
	/// </summary>
	public static void Reset()
	{
		estaPausado	= false;
		tiempoUI	= 1.0f;
		tiempoJuego = 1.0f;
	}
}
