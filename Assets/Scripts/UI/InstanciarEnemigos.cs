﻿using UnityEngine;
using System.Collections;

public class InstanciarEnemigos : MonoBehaviour
{
	public Transform[]		pointsToInstantiate;
	public GameObject[]		enemies;

	public float			minTimeToCreate 	= 1.0f;
	public float			maxTimeToCreate 	= 1.5f;
	
	private int				idLastPointInstantiate	= -1;
	private int 			idLastEnemyInstantiate	= -1;
	private float 			timeToCreate		= 0.0f;
	private bool 			createInfinitly = true;
	
//	IEnumerator Start()
//	{
//		yield return StartCoroutine ( WaitAndCreate() );
//	}
//	
//	IEnumerator WaitAndCreate()
//	{
//		do
//		{
//			if (UnityUtility.IsPaused)
//			{
//				yield return new WaitForSeconds(0.0f);
//			}
//			else
//			{
//				if (!UnityUtility.GameOver)
//				{
//					timeToCreate = Random.Range(minTimeToCreate, maxTimeToCreate);
//					yield return new WaitForSeconds( timeToCreate );
//					InstantiateEnemy();
//				}
//				else
//				{
//					
//					StopAllCoroutines();
//					Destroy(this.gameObject);
//					createInfinitly = false;
//				}
//			}
//		} while(createInfinitly);
//	}
//	
//	void InstantiateEnemy()
//	{
//		idLastPointInstantiate = Random.Range (0, pointsToInstantiate.Length);
//		idLastEnemyInstantiate = Random.Range (0, enemies.Length);
//		
//		// Se asigna el objeto a la plataforma instanciada
//		GameObject instantiatePlatform = (GameObject)Instantiate (enemies [idLastEnemyInstantiate], pointsToInstantiate [idLastPointInstantiate].position, Quaternion.identity);
//
//		Debug.LogError("Intancio");
//	}
}
