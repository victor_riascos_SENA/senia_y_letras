﻿using UnityEngine;
using System.Collections;

public class UI_Creditos : MonoBehaviour {

    public void Btn_Cancelar()
    {
        HUDManager.Instance.pantallaActual = Pantalla.Diccionario;
        HUDManager.Instance.ActivarPantallaCredito(false);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);
    }

    public void Btn_Aceptar()
    {
        HUDManager.Instance.pantallaActual = Pantalla.Menu;
        HUDManager.Instance.ActivarPantallaCredito(true);
        SoundManager.Instance.PlayUISounds(UISounds.Button, true);
    }
}
