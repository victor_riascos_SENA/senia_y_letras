﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI_tools : MonoBehaviour {

    bool configuracion_Activa = false;
    public Toggle toggleSonido;
    public Toggle toggleMusica;
    public Image imagenToggleSonido;
    public Image imagenToggleMusica;
    public bool activar_sonido = true;
   


    public void Btn_ActivarTools()
    {
        configuracion_Activa = !configuracion_Activa;
        HUDManager.Instance.pantallaActual = Pantalla.Menu;
        HUDManager.Instance.ActivarPanelTools(configuracion_Activa);
               
        SoundManager.Instance.PlayUISounds(UISounds.Button , true); 
       
    }

    public void Cambiar_Imag_Sonido()
    {
        GameInformation.SeniasYLetras.Sonido = toggleSonido.isOn;

        if (toggleSonido.isOn )
        {
           SoundManager.Instance.PlayUISounds(UISounds.Button, true);
            imagenToggleSonido.sprite = Resources.Load<Sprite>("UI/Iconos/sound_on");
           
        }

        else
        {
            imagenToggleSonido.sprite = Resources.Load<Sprite>("UI/Iconos/sound_off");
            SoundManager.Instance.PlayUISounds(UISounds.Button, false);
        }
        
        
       

    }

    public void Cambiar_Imag_Musica()
    {
        
        if (toggleMusica.isOn)
        {
           
            imagenToggleMusica.sprite = Resources.Load<Sprite>("UI/Iconos/Music_on");
        }
            
        else
        {
            imagenToggleMusica.sprite = Resources.Load<Sprite>("UI/Iconos/Music_off");
            
        }
        SoundManager.Instance.PlayMusic(toggleMusica.isOn);
    }

}
