﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class UI_BtnsInterprete : MonoBehaviour 
{
	
	public InputField inputInterprete;
	public Text NombreAnimacion;
	public bool puedeAnimar;


	public void BtnAnimar ()
	{
		if (inputInterprete.text != "") 
		{
			StartCoroutine (EsperarAnimacion ());
		}
	}

	public void BtnLimpiar ()
	{
		inputInterprete.text = "";
	}

	private string PalabraTipoOracion(string palabra)
	{	
		if (palabra == "") return "";
		return palabra.First().ToString().ToUpper() + palabra.Substring(1).ToLower();
	}

	void AnimarPalabra(Animacion P_Actual)
	{
		PersonajeManager.Instance.Animar (P_Actual.Id);
	}

	IEnumerator IniciarCorrutina()
	{
		Debug.LogError ("Inicio Corrutina");
		yield return StartCoroutine (EsperarAnimacion ());
		Debug.LogError ("Finalizo Corrutina");
	}

	IEnumerator EsperarAnimacion()
	{
		string[] arregloPalabras = inputInterprete.text.Split (' '); 

		foreach (string palabraActual in arregloPalabras) 
		{
			Animacion animacionActual = GameInformation.SeniasYLetras.Avatar.Animaciones [ this.PalabraTipoOracion(palabraActual) ];
			// Anime al personaje con la palabra del diccionario
			if (animacionActual != null)
			{
				PersonajeManager.Instance.animatorPersonaje.SetBool ("animar", false);
				PersonajeManager.Instance.Animar (animacionActual.Id);

				float tiempoAnimacion = PersonajeManager.Instance.ObtenerTiempoAnimacion ( animacionActual.Nombre );
				tiempoAnimacion = ((tiempoAnimacion > 1) ? tiempoAnimacion : 1.5f )/UnityUtility.TiempoJuego;

				//Debug.LogError ( tiempoAnimacion );

				NombreAnimacion.text = this.PalabraTipoOracion (palabraActual);
				yield return new WaitForSeconds (tiempoAnimacion);
			}
			/*else
			{
				char[] palabraDeletreada = palabraActual.ToCharArray ();
					foreach (string l in arregloPalabras) |
					Debug.LogError (l);	
				yield return new WaitForSeconds (5.0f);
			}*/
		}
		yield return null;
			
	}
}
