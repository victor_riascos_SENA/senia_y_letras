﻿using UnityEngine;
using System.Collections;
//using UnityEngine;
/* Importación de librerias necesarias para serializar objetos */
using System;
using System.IO;
using System.Xml.Serialization;

public static class XMLManager
{
	/// <summary>
	/// Guarda en un archivo de tipo Xml la estructura de la clase que se desea guardar (SIN ENCRIPTAR).
	/// </summary>
	/// <param name="tipoObjeto">Se define el "tipo" de objeto que se desea guardar.</param>
	/// <param name="objeto">Se define que recibe el objeto a guardar.</param>
	/// <param name="nombreArchivo">Se define el nombre del archivo que dará persistencia al objeto en memoria.</param>
	public static void Serialize(Type objectType, object objectReference, string fileName)
	{
		XmlSerializer xmlSerializer = new XmlSerializer(objectType);
		TextWriter textWritter = new StreamWriter(fileName);
		
		xmlSerializer.Serialize(textWritter, objectReference);
		
		textWritter.Close ();

	}
	
	
	/// <summary>
	/// Guarda en un objeto en memoria la estructura de un archivo Xml del tipo especificado (SIN ENCRIPTAR).
	/// </summary>
	/// <param name="tipoObjecto">Se define el "tipo" de objeto que se desea leer.</param>
	/// <param name="nombreArchivo">Se define el nombre del archivo del cual se desea obtener el contenido en memoria.</param>
	/// <returns></returns>
	public static object Deserialize(Type objectType, string fileName)
	{
		if (!File.Exists(fileName)) return null;
		
		XmlSerializer xmlSerializer = new XmlSerializer(objectType);
		TextReader textReader = new StreamReader(fileName);
		
		object objectXml = xmlSerializer.Deserialize(textReader);
		textReader.Close();
		
		return objectXml;
	}
}
