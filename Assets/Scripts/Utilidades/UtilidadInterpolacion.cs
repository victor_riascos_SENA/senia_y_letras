﻿using UnityEngine;

public class UtilidadInterpolacion
{
    private static float rate = 0.0f;
    private static float i = 0.0f;

	/// <summary>
	/// Metodo que permite restablecer los valores por defecto de la interpolacion
	/// </summary>
    public static void InicializarInterpolacion()
    {
        rate = 0.0f;
        i = 0.0f;
    }

    public static Vector3 Interpolar(Vector3 valorActual, Vector3 valorFinal, float tiempo)
    {
        rate = 1.0f / tiempo;
        if (i < 1.0)
        {
            i += Time.deltaTime * rate;
            return Vector3.Lerp(valorActual, valorFinal, i);
        }
        return valorFinal;
    }

    public static float Interpolar(float valorActual, float valorFinal, float tiempo)
    {
        rate = 1.0f / tiempo;
        if (i < 1.0)
        {
            i += Time.deltaTime * rate;
            return Mathf.Lerp(valorActual, valorFinal, i);
        }
        return valorFinal;
    }

    public static Quaternion Interpolar(Quaternion valorActual, Quaternion valorFinal, float tiempo)
    {
        rate = 1.0f / tiempo;
        if (i < 1.0)
        {
            i += Time.deltaTime * rate;
            return Quaternion.Lerp(valorActual, valorFinal, i);
        }
        return valorFinal;
    }

    public static int Interpolar(int valorActual, int valorFinal, float tiempo)
    {
        rate = 1.0f / tiempo;
        if (i < 1.0)
        {
            i += Time.deltaTime * rate;
            return (int)Mathf.Lerp(valorActual, valorFinal, i);
        }
        return valorFinal;
    }
}
