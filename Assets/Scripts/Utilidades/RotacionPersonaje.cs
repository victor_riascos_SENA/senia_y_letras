﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RotacionPersonaje : MonoBehaviour {

	public Scrollbar  scrollbar;
	public float 	  velocidadRotacion = 0.0f;

	private Vector3   vectorRotacion = Vector3.zero;
	private float 	  speed = 2.0f;
	private float     valorScroll;
	private float 	  distancia =0.0f;
	private float 	  distanciaAnt =0.0f;

	public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
	public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode

	void Start () 
	{	
		distanciaAnt = distancia;
		vectorRotacion = this.transform.rotation.eulerAngles;
	}
	
	void Update ()
	{
		if (Input.touchCount == 1) {
			if (Input.GetKey (KeyCode.Mouse0) && Input.GetTouch (0).phase == TouchPhase.Moved ) {
				vectorRotacion = new Vector3 (0.0f, vectorRotacion.x + -Input.GetAxis ("Mouse X") * 5, 0.0f);
				this.transform.Rotate (vectorRotacion * velocidadRotacion * Time.deltaTime, Space.World);
			}
		}

		if (Input.GetKey (KeyCode.Mouse0) && Application.platform != RuntimePlatform.Android) {
				vectorRotacion = new Vector3 (0.0f, vectorRotacion.x + -Input.GetAxis ("Mouse X") * 5, 0.0f);
				this.transform.Rotate (vectorRotacion * velocidadRotacion * Time.deltaTime, Space.World);
			}
		

		/*if (Input.GetKey (KeyCode.Mouse1) && Input.GetKey (KeyCode.Space)) 
		{
			Debug.LogError ("esta pulsando 2 botones");
			 valorScroll = scrollbar.value;

			scrollbar.value += Input.GetAxis ("Mouse X")*Time.deltaTime ;
		}*/


	}
}
